#include "linmessage.h"


LinMessage::LinMessage(QObject *parent) : QObject(parent) {
    data = new QList<unsigned char>();
}

LinMessage::~LinMessage() {
    delete data;
}

unsigned short LinMessage::getId() const {
    return id;
}

void LinMessage::setId(unsigned short value) {
    id = value;
}

QList<unsigned char> *LinMessage::getData() const {
    return data;
}

void LinMessage::setData(QList<unsigned char> *value) {
    delete data;
    data = value;
}

unsigned char LinMessage::getCrc() const {
    return crc;
}

void LinMessage::setCrc(unsigned char value) {
    crc = value;
}
