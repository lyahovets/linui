#ifndef LINMESSAGE_H
#define LINMESSAGE_H

#include <QObject>

#define LIN_DATA_ARRAY_SIZE     8

class LinMessage : public QObject {
    Q_OBJECT
public:
    explicit LinMessage(QObject *parent = 0);
    ~LinMessage();

    unsigned short getId() const;
    void setId(unsigned short value);

    QList<unsigned char> *getData() const;
    void setData(QList<unsigned char> *value);

    unsigned char getCrc() const;
    void setCrc(unsigned char value);

private:
    unsigned short id;
    QList<unsigned char> *data;
    unsigned char *pData;
    unsigned char crc;

signals:

public slots:

};

#endif // LINMESSAGE_H
