#include "datatablemodel.h"

#define __DATA_TABLE_COLUM_COUNT                10
#define __DATA_TABLE_ID_COLUMN_NUMBER           0
#define __DATA_TABLE_BYTE0_COLUMN_NUMBER        1
#define __DATA_TABLE_BYTE1_COLUMN_NUMBER        2
#define __DATA_TABLE_BYTE2_COLUMN_NUMBER        3
#define __DATA_TABLE_BYTE3_COLUMN_NUMBER        4
#define __DATA_TABLE_BYTE4_COLUMN_NUMBER        5
#define __DATA_TABLE_BYTE5_COLUMN_NUMBER        6
#define __DATA_TABLE_BYTE6_COLUMN_NUMBER        7
#define __DATA_TABLE_BYTE7_COLUMN_NUMBER        8
#define __DATA_TABLE_CRC_COLUMN_NUMBER          9

DataTableModel::DataTableModel(QObject *parent) : QAbstractTableModel(parent) {
    dataList = new QList<LinMessage *>();
}

DataTableModel::~DataTableModel() {
    destoryData();
}

void DataTableModel::addEntry(LinMessage *entry) {
    beginInsertRows(QModelIndex(), 0, 0);
    dataList->insert(0, entry);
    endInsertRows();
}

void DataTableModel::setEntryList(QList<LinMessage *> *entryList) {
    beginResetModel();
    destoryData();
    dataList = entryList;
    endResetModel();
}

QString DataTableModel::numberToHex(int value) const {
    return QString::number(value, 16);
}

void DataTableModel::destoryData() {
    for (int i = 0; i < dataList->size(); i++) {
        delete dataList->at(i);
    }
    delete dataList;
}

int DataTableModel::rowCount(const QModelIndex &) const {
    return dataList->size();
}

int DataTableModel::columnCount(const QModelIndex &) const {
    return __DATA_TABLE_COLUM_COUNT;
}

QVariant DataTableModel::data(const QModelIndex &index, int role) const {
    switch (role) {
    case Qt::DisplayRole:
    {
        LinMessage *element = dataList->at(index.row());
        if (index.column() == __DATA_TABLE_ID_COLUMN_NUMBER) {
            return numberToHex(element->getId());
        } else if (index.column() == __DATA_TABLE_CRC_COLUMN_NUMBER) {
            return numberToHex(element->getCrc());
        } else {
            if (element->getData()->size() >= index.column()) {
                return numberToHex(element->getData()->at(index.column() - 1));
            }
        }
        break;
    }
    default:
        return QVariant();
        break;
    }
    return QVariant();
}

QVariant DataTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    switch (orientation) {
//    case Qt::Vertical:
//        switch (role) {
//        case Qt::DisplayRole:
//            return section;
//            break;
//        default:
//            break;
//        }
//        break;
    case Qt::Horizontal:
        switch (role) {
        case Qt::DisplayRole:
            switch (section) {
            case __DATA_TABLE_ID_COLUMN_NUMBER:
                return "ID";
                break;
            case __DATA_TABLE_BYTE0_COLUMN_NUMBER:
                return "BYTE0";
                break;
            case __DATA_TABLE_BYTE1_COLUMN_NUMBER:
                return "BYTE1";
                break;
            case __DATA_TABLE_BYTE2_COLUMN_NUMBER:
                return "BYTE2";
                break;
            case __DATA_TABLE_BYTE3_COLUMN_NUMBER:
                return "BYTE3";
                break;
            case __DATA_TABLE_BYTE4_COLUMN_NUMBER:
                return "BYTE4";
                break;
            case __DATA_TABLE_BYTE5_COLUMN_NUMBER:
                return "BYTE5";
                break;
            case __DATA_TABLE_BYTE6_COLUMN_NUMBER:
                return "BYTE6";
                break;
            case __DATA_TABLE_BYTE7_COLUMN_NUMBER:
                return "BYTE7";
                break;
            case __DATA_TABLE_CRC_COLUMN_NUMBER:
                return "CRC";
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        break;
    default:
        return QVariant();
        break;
    }
    return QVariant();
}


