#ifndef DATATABLEMODEL_H
#define DATATABLEMODEL_H

#include <QAbstractTableModel>
#include <entity/linmessage.h>


class DataTableModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit DataTableModel(QObject *parent = 0);
    ~DataTableModel();
    virtual void addEntry(LinMessage *entry);
    virtual void setEntryList(QList<LinMessage *> *entryList);

private:
    QList<LinMessage *> *dataList;
    QString numberToHex(int value) const;
    void destoryData();

protected:
    virtual int rowCount(const QModelIndex &) const override;
    virtual int columnCount(const QModelIndex &) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

signals:

public slots:

};

#endif // DATATABLEMODEL_H
