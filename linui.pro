#-------------------------------------------------
#
# Project created by QtCreator 2017-08-16T08:28:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = linui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    data_model/datatablemodel.cpp \
    entity/linmessage.cpp \
    view/datamonitorview.cpp \
    entity/datamonitor.cpp

HEADERS  += mainwindow.h \
    data_model/datatablemodel.h \
    entity/linmessage.h \
    view/datamonitorview.h \
    entity/datamonitor.h

RESOURCES += \
    res.qrc

DISTFILES +=
