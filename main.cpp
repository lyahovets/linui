#include "mainwindow.h"
#include <QApplication>
#include <QObject>
#include <QFile>


int main(int argc, char *argv[]) {

    QApplication a(argc, argv);

    QFile styleFile(":/theme/style.css");
    if (styleFile.exists()) {
        if (styleFile.open(QFile::ReadOnly)) {
            QString styleData = styleFile.readAll();
            a.setStyleSheet(styleData);
        }
    }

    MainWindow w;
    w.show();

    return a.exec();
}
