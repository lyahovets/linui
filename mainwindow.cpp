#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    setMinimumSize(800, 600);
    setCentralWidget(new DataMonitorView(this));

}

MainWindow::~MainWindow() {

}
