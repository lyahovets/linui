#include "datamonitorview.h"

DataMonitorView::DataMonitorView(QWidget *parent) : QWidget(parent) {
    columnLayout = new QHBoxLayout(this);
    columnLayout->setMargin(1);

    QWidget *controllPanelProxy = new QWidget(this);
    controllPanelProxy->setMinimumWidth(200);
    controllPanelProxy->setMaximumWidth(300);
    columnLayout->addWidget(controllPanelProxy);

    controllLayout = new QGridLayout(controllPanelProxy);
    controllLayout->setMargin(0);
    controllLayout->addWidget(new QPushButton("1", controllPanelProxy));
    controllLayout->addWidget(new QPushButton("2", controllPanelProxy));
    controllLayout->addWidget(new QPushButton("3", controllPanelProxy));
    controllLayout->addWidget(new QPushButton("4", controllPanelProxy));
    controllLayout->addWidget(new QPushButton("5", controllPanelProxy));

    QWidget *dataViewProxyPanel = new QWidget(this);
    dataViewProxyPanel->setMinimumWidth(600);
    columnLayout->addWidget(dataViewProxyPanel);
    columnLayout->setStretch(0, 1);
    columnLayout->setStretch(1, 3);

    dataRowLayout = new QVBoxLayout(dataViewProxyPanel);
    dataRowLayout->setMargin(0);

    messageCompact = new QTableView(dataViewProxyPanel);
    dataRowLayout->addWidget(messageCompact);

    messageLog = new QTableView(dataViewProxyPanel);
    messageLogModel = new DataTableModel(this);
    messageLog->setModel(messageLogModel);
    dataRowLayout->addWidget(messageLog);

    dataRowLayout->setStretch(0, 2);
    dataRowLayout->setStretch(1, 1);

    testTimer = new QTimer(this);
    connect(testTimer, SIGNAL(timeout()), this, SLOT(addEntry()));
    testTimer->start(1000);
}

DataMonitorView::~DataMonitorView() {

}

void DataMonitorView::addEntry() {
    LinMessage *message = new LinMessage();
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->getData()->append(qrand() % 0xFF);
    message->setId(qrand() % 0xFF);
    message->setCrc(qrand() % 0xFF);

    messageLogModel->addEntry(message);
}
