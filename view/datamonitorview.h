#ifndef DATAMONITORVIEW_H
#define DATAMONITORVIEW_H

#include <QWidget>
#include <QGridLayout>
#include <QTableView>
#include <QHeaderView>
#include <QPushButton>
#include <QTextEdit>
#include <data_model/datatablemodel.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QTimer>


class DataMonitorView : public QWidget {

    Q_OBJECT

public:
    explicit DataMonitorView(QWidget *parent = 0);
    ~DataMonitorView();

private:
    QHBoxLayout *columnLayout;
    QGridLayout *controllLayout;
    QVBoxLayout *dataRowLayout;

    QTableView *messageLog;
    QTableView *messageCompact;
    QPushButton *startButton;
    QPushButton *stopButton;
    DataTableModel *messageLogModel;

    QTimer *testTimer;

signals:

public slots:

private slots:
    void addEntry();

};

#endif // DATAMONITORVIEW_H
